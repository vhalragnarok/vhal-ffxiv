@ echo off

@ set REG1=HKLM\SOFTWARE\JavaSoft\Java Runtime Environment
@ set REG2=HKLM\SOFTWARE\JavaSoft\Java Development Kit
@ set REG3=HKLM\SOFTWARE\JavaSoft\JDK

@ set REG=%REG1%
@ FOR /F "tokens=2* delims=	 " %%A IN ('REG QUERY "%REG1%" /v "CurrentVersion"') DO SET ED=%%B

@ IF "%ED%" == "" (
  @ set REG=%REG2%
  @ FOR /F "tokens=2* delims=	 " %%A IN ('REG QUERY "%REG2%" /v "CurrentVersion"') DO SET ED=%%B
)

@ IF "%ED%" == "" (
  @ set REG=%REG3%
  @ FOR /F "tokens=2* delims=	 " %%A IN ('REG QUERY "%REG3%" /v "CurrentVersion"') DO SET ED=%%B
)

@ FOR /F "tokens=2* delims=	 " %%A IN ('REG QUERY "%REG%\%ED%" /v "JavaHome"') DO SET PF=%%B

@ rem echo [ %REG% ]
@ rem echo [ %ED% ]
@ rem echo [ %PF% ]

@ "%PF%\bin\java.exe" -Xms1024m -Xmx1024m -jar GDStash.jar
