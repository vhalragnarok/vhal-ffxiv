17:48:42.857: [Safe Mode] Unclean shutdown detected!
17:48:44.368: [Safe Mode] User elected to launch normally.
17:48:44.368: Platform: Wayland
17:48:44.368: CPU Name: AMD Ryzen 9 7950X3D 16-Core Processor
17:48:44.368: CPU Speed: 3895.191MHz
17:48:44.369: Physical Cores: 16, Logical Cores: 32
17:48:44.369: Physical Memory: 63402MB Total, 53270MB Free
17:48:44.369: Kernel Version: Linux 6.6.11-1-default
17:48:44.369: Flatpak Branch: stable
17:48:44.369: Flatpak Arch: x86_64
17:48:44.369: Flatpak Runtime: runtime/org.kde.Platform/x86_64/6.5
17:48:44.369: App Extensions:
17:48:44.369:  - com.obsproject.Studio.Locale
17:48:44.369: Runtime Extensions:
17:48:44.369:  - org.freedesktop.Platform.GL.default
17:48:44.369:  - org.freedesktop.Platform.openh264
17:48:44.369:  - org.gtk.Gtk3theme.Breeze
17:48:44.369:  - org.kde.Platform.Locale
17:48:44.369:  - org.freedesktop.Platform.GL.default
17:48:44.369: Flatpak Framework Version: 1.15.6
17:48:44.369: Desktop Environment: KDE (KDE)
17:48:44.369: Session Type: wayland
17:48:44.370: Qt Version: 6.5.3 (runtime), 6.5.3 (compiled)
17:48:44.370: Portable mode: false
17:48:44.394: OBS 30.0.2 (linux)
17:48:44.394: ---------------------------------
17:48:44.394: ---------------------------------
17:48:44.394: audio settings reset:
17:48:44.394: 	samples per sec: 48000
17:48:44.394: 	speakers:        2
17:48:44.394: 	max buffering:   960 milliseconds
17:48:44.394: 	buffering type:  dynamically increasing
17:48:44.400: ---------------------------------
17:48:44.400: Initializing OpenGL...
17:48:44.400: Using EGL/Wayland
17:48:44.400: Initialized EGL 1.5
17:48:44.410: Loading up OpenGL on adapter AMD AMD Radeon RX 7900 XTX (radeonsi, navi31, LLVM 15.0.7, DRM 3.54, 6.6.11-1-default)
17:48:44.410: OpenGL loaded successfully, version 4.6 (Core Profile) Mesa 23.3.0 (git-1fbdd37d4c), shading language 4.60
17:48:44.446: ---------------------------------
17:48:44.446: video settings reset:
17:48:44.446: 	base resolution:   3440x1440
17:48:44.446: 	output resolution: 3440x1440
17:48:44.446: 	downscale filter:  Bicubic
17:48:44.446: 	fps:               60/1
17:48:44.446: 	format:            NV12
17:48:44.446: 	YUV mode:          Rec. 709/Partial
17:48:44.446: NV12 texture support not available
17:48:44.446: P010 texture support not available
17:48:44.448: Audio monitoring device:
17:48:44.448: 	name: Default
17:48:44.448: 	id: default
17:48:44.448: ---------------------------------
17:48:44.464: No AJA devices found, skipping loading AJA UI plugin
17:48:44.464: Failed to initialize module 'aja-output-ui.so'
17:48:44.477: No AJA devices found, skipping loading AJA plugin
17:48:44.477: Failed to initialize module 'aja.so'
17:48:44.481: Failed to load 'en-US' text for module: 'decklink-captions.so'
17:48:44.487: Failed to load 'en-US' text for module: 'decklink-output-ui.so'
17:48:44.492: A DeckLink iterator could not be created.  The DeckLink drivers may not be installed
17:48:44.492: Failed to initialize module 'decklink.so'
17:48:44.658: [pipewire] Available captures:
17:48:44.658: [pipewire]     - Desktop capture
17:48:44.658: [pipewire]     - Window capture
17:48:44.680: v4l2loopback not installed, virtual camera disabled
17:48:44.687: [obs-browser]: Version 2.22.2
17:48:44.687: [obs-browser]: CEF Version 103.0.5060.134 (runtime), 103.0.0-5060-shared-textures.2594+g17f8588+chromium-103.0.5060.134 (compiled)
17:48:44.714: VAAPI: API version 1.18
17:48:44.715: FFmpeg VAAPI H264 encoding supported
17:48:44.721: FFmpeg VAAPI HEVC encoding supported
17:48:44.788: [obs-websocket] [obs_module_load] you can haz websockets (Version: 5.3.4 | RPC Version: 1)
17:48:44.788: [obs-websocket] [obs_module_load] Qt version (compile-time): 6.5.3 | Qt version (run-time): 6.5.3
17:48:44.788: [obs-websocket] [obs_module_load] Linked ASIO Version: 102800
17:48:44.791: [obs-websocket] [obs_module_load] Module loaded.
17:48:44.806: ---------------------------------
17:48:44.806:   Loaded Modules:
17:48:44.806:     text-freetype2.so
17:48:44.806:     rtmp-services.so
17:48:44.806:     obs-x264.so
17:48:44.806:     obs-websocket.so
17:48:44.806:     obs-webrtc.so
17:48:44.806:     obs-vst.so
17:48:44.806:     obs-transitions.so
17:48:44.806:     obs-qsv11.so
17:48:44.806:     obs-outputs.so
17:48:44.806:     obs-libfdk.so
17:48:44.806:     obs-filters.so
17:48:44.806:     obs-ffmpeg.so
17:48:44.806:     obs-browser.so
17:48:44.806:     linux-v4l2.so
17:48:44.806:     linux-pulseaudio.so
17:48:44.806:     linux-pipewire.so
17:48:44.806:     linux-jack.so
17:48:44.806:     linux-capture.so
17:48:44.806:     image-source.so
17:48:44.806:     frontend-tools.so
17:48:44.806:     decklink-output-ui.so
17:48:44.806:     decklink-captions.so
17:48:44.806: ---------------------------------
17:48:44.806: ==== Startup complete ===============================================
17:48:44.842: All scene data cleared
17:48:44.842: ------------------------------------------------
17:48:44.846: pulse-input: Server name: 'PulseAudio (on PipeWire 1.0.1) 15.0.0'
17:48:44.846: pulse-input: Audio format: s24le, 48000 Hz, 2 channels
17:48:44.846: pulse-input: Sample format s24le not supported by OBS,using float32le instead for recording
17:48:44.846: pulse-input: Started recording from 'alsa_output.usb-Mayflower_Electronics_ARC_AMP_DAC_ARC_AMP_DAC_FFFFFFFEFFFF-00.analog-stereo.monitor' (default)
17:48:44.846: [Loaded global audio device]: 'Desktop Audio'
17:48:44.847: pulse-input: Server name: 'PulseAudio (on PipeWire 1.0.1) 15.0.0'
17:48:44.847: pulse-input: Audio format: s24le, 48000 Hz, 1 channels
17:48:44.847: pulse-input: Sample format s24le not supported by OBS,using float32le instead for recording
17:48:44.847: pulse-input: Started recording from 'alsa_input.usb-Sennheiser_Sennheiser_Profile_0363012587-00.mono-fallback'
17:48:44.847: [Loaded global audio device]: 'Mic/Aux'
17:48:44.847: PipeWire initialized
17:48:44.847: PipeWire initialized
17:48:44.848: Switched to scene 'Scene'
17:48:44.848: ------------------------------------------------
17:48:44.848: Loaded scenes:
17:48:44.848: - scene 'Scene':
17:48:44.848:     - source: 'FFXIV' (pipewire-window-capture-source)
17:48:44.848:     - source: 'Spotify' (pipewire-window-capture-source)
17:48:44.848: - scene 'Break':
17:48:44.848:     - source: 'Break Message' (text_ft2_source_v2)
17:48:44.848:     - source: 'Spotify' (pipewire-window-capture-source)
17:48:44.848: ------------------------------------------------
17:48:44.867: [pipewire] Screencast session created
17:48:44.867: [pipewire] Screencast session created
17:48:44.876: [pipewire] Asking for window
17:48:44.876: [pipewire] Asking for window
17:48:45.418: adding 21 milliseconds of audio buffering, total audio buffering is now 21 milliseconds (source: Desktop Audio)
17:48:45.418: 
17:48:45.440: adding 21 milliseconds of audio buffering, total audio buffering is now 42 milliseconds (source: Mic/Aux)
17:48:45.440: 
17:48:46.342: [pipewire] Failed to start screencast, denied or cancelled by user
17:48:46.492: [pipewire] Failed to start screencast, denied or cancelled by user
17:48:48.692: PipeWire initialized
17:48:48.694: [pipewire] Screencast session created
17:48:48.695: [pipewire] Asking for window
17:48:50.066: [pipewire] window selected, setting up screencast
17:48:50.082: [pipewire] Server version: 1.0.1
17:48:50.082: [pipewire] Library version: 0.3.65
17:48:50.082: [pipewire] Header version: 0.3.65
17:48:50.082: [pipewire] Created stream 0x557061352bc0
17:48:50.082: [pipewire] Stream 0x557061352bc0 state: "connecting" (error: none)
17:48:50.083: [pipewire] Playing stream 0x557061352bc0
17:48:50.083: [pipewire] Stream 0x557061352bc0 state: "paused" (error: none)
17:48:50.084: [pipewire] Negotiated format:
17:48:50.084: [pipewire]     Format: 12 (Spa:Enum:VideoFormat:BGRA)
17:48:50.084: [pipewire]     Modifier: 0x0
17:48:50.084: [pipewire]     Size: 1712x1368
17:48:50.084: [pipewire]     Framerate: 0/1
17:48:50.084: [pipewire] Negotiated format:
17:48:50.084: [pipewire]     Format: 12 (Spa:Enum:VideoFormat:BGRA)
17:48:50.084: [pipewire]     Modifier: 0x200000028a6bf04
17:48:50.084: [pipewire]     Size: 1712x1368
17:48:50.084: [pipewire]     Framerate: 0/1
17:48:50.138: [pipewire] Stream 0x557061352bc0 state: "streaming" (error: none)
17:48:52.381: PipeWire initialized
17:48:52.383: [pipewire] Screencast session created
17:48:52.384: [pipewire] Asking for window
17:48:55.509: [pipewire] window selected, setting up screencast
17:48:55.511: [pipewire] Server version: 1.0.1
17:48:55.511: [pipewire] Library version: 0.3.65
17:48:55.511: [pipewire] Header version: 0.3.65
17:48:55.511: [pipewire] Created stream 0x55706138a7d0
17:48:55.511: [pipewire] Stream 0x55706138a7d0 state: "connecting" (error: none)
17:48:55.511: [pipewire] Playing stream 0x55706138a7d0
17:48:55.511: [pipewire] Stream 0x55706138a7d0 state: "paused" (error: none)
17:48:55.512: [pipewire] Negotiated format:
17:48:55.512: [pipewire]     Format: 12 (Spa:Enum:VideoFormat:BGRA)
17:48:55.512: [pipewire]     Modifier: 0x0
17:48:55.512: [pipewire]     Size: 3440x1440
17:48:55.512: [pipewire]     Framerate: 0/1
17:48:55.512: [pipewire] Negotiated format:
17:48:55.512: [pipewire]     Format: 12 (Spa:Enum:VideoFormat:BGRA)
17:48:55.512: [pipewire]     Modifier: 0x200000028a37f04
17:48:55.512: [pipewire]     Size: 3440x1440
17:48:55.512: [pipewire]     Framerate: 0/1
17:48:55.538: [pipewire] Stream 0x55706138a7d0 state: "streaming" (error: none)
18:03:38.874: ---------------------------------
18:03:38.874: [x264 encoder: 'simple_video_stream'] preset: veryfast
18:03:38.874: [x264 encoder: 'simple_video_stream'] settings:
18:03:38.874: 	rate_control: CBR
18:03:38.874: 	bitrate:      51000
18:03:38.874: 	buffer size:  51000
18:03:38.874: 	crf:          23
18:03:38.874: 	fps_num:      60
18:03:38.874: 	fps_den:      1
18:03:38.874: 	width:        3440
18:03:38.874: 	height:       1440
18:03:38.874: 	keyint:       120
18:03:38.874: 
18:03:38.890: libfdk_aac encoder created
18:03:38.890: libfdk_aac bitrate: 160, channels: 2
18:03:38.890: [rtmp stream: 'simple_stream'] Connecting to RTMP URL rtmps://a.rtmps.youtube.com:443/live2...
18:03:39.364: [rtmp stream: 'simple_stream'] Connection to rtmps://a.rtmps.youtube.com:443/live2 (172.217.4.204) successful
18:03:39.366: ==== Streaming Start ===============================================
18:04:09.366: Failed to create xdg-screensaver: 2
18:04:39.366: Failed to create xdg-screensaver: 2
18:05:09.369: Failed to create xdg-screensaver: 2
18:05:39.369: Failed to create xdg-screensaver: 2
18:06:09.370: Failed to create xdg-screensaver: 2
18:06:39.370: Failed to create xdg-screensaver: 2
18:07:09.371: Failed to create xdg-screensaver: 2
18:07:39.371: Failed to create xdg-screensaver: 2
18:08:09.372: Failed to create xdg-screensaver: 2
18:08:39.372: Failed to create xdg-screensaver: 2
18:09:09.373: Failed to create xdg-screensaver: 2
18:09:39.373: Failed to create xdg-screensaver: 2
18:10:09.373: Failed to create xdg-screensaver: 2
18:10:39.374: Failed to create xdg-screensaver: 2
18:11:09.374: Failed to create xdg-screensaver: 2
18:11:39.375: Failed to create xdg-screensaver: 2
18:12:09.375: Failed to create xdg-screensaver: 2
18:12:39.376: Failed to create xdg-screensaver: 2
18:13:09.376: Failed to create xdg-screensaver: 2
18:13:39.376: Failed to create xdg-screensaver: 2
18:14:09.377: Failed to create xdg-screensaver: 2
18:14:39.377: Failed to create xdg-screensaver: 2
18:15:09.378: Failed to create xdg-screensaver: 2
18:15:39.378: Failed to create xdg-screensaver: 2
18:16:09.379: Failed to create xdg-screensaver: 2
18:16:39.379: Failed to create xdg-screensaver: 2
18:17:09.380: Failed to create xdg-screensaver: 2
18:17:39.380: Failed to create xdg-screensaver: 2
18:18:09.380: Failed to create xdg-screensaver: 2
18:18:39.381: Failed to create xdg-screensaver: 2
18:19:09.381: Failed to create xdg-screensaver: 2
21:27:22.483: Last log entry repeated for 376 more lines
21:27:22.483: [rtmp stream: 'simple_stream'] User stopped the stream
21:27:22.483: Output 'simple_stream': stopping
21:27:22.483: Output 'simple_stream': Total frames output: 733328
21:27:22.483: Output 'simple_stream': Total drawn frames: 733415 (733416 attempted)
21:27:22.483: Output 'simple_stream': Number of lagged frames due to rendering lag/stalls: 1 (0.0%)
21:27:22.484: ==== Streaming Stop ================================================
21:27:22.516: libfdk_aac encoder destroyed
23:04:13.158: ==== Shutting down ==================================================
23:04:13.160: pulse-input: Stopped recording from 'alsa_output.usb-Mayflower_Electronics_ARC_AMP_DAC_ARC_AMP_DAC_FFFFFFFEFFFF-00.analog-stereo.monitor'
23:04:13.160: pulse-input: Got 756219 packets with 907462800 frames
23:04:13.160: pulse-input: Stopped recording from 'alsa_input.usb-Sennheiser_Sennheiser_Profile_0363012587-00.mono-fallback'
23:04:13.160: pulse-input: Got 757098 packets with 908517600 frames
23:04:13.160: [pipewire] Stream 0x55706138a7d0 state: "paused" (error: none)
23:04:13.161: [pipewire] Stream 0x55706138a7d0 state: "unconnected" (error: none)
23:04:13.161: [pipewire] Stream 0x557061352bc0 state: "paused" (error: none)
23:04:13.161: [pipewire] Stream 0x557061352bc0 state: "unconnected" (error: none)
23:04:13.184: All scene data cleared
23:04:13.184: ------------------------------------------------
23:04:13.227: [obs-websocket] [obs_module_unload] Shutting down...
23:04:13.227: Tried to call obs_frontend_remove_event_callback with no callbacks!
23:04:13.227: [obs-websocket] [obs_module_unload] Finished shutting down.
23:04:13.231: [Scripting] Total detached callbacks: 0
23:04:13.231: Freeing OBS context data
23:04:13.238: == Profiler Results =============================
23:04:13.238: run_program_init: 2053 ms
23:04:13.238:  ┣OBSApp::AppInit: 4.385 ms
23:04:13.238:  ┃ ┗OBSApp::InitLocale: 1.514 ms
23:04:13.238:  ┗OBSApp::OBSInit: 495.655 ms
23:04:13.238:    ┣obs_startup: 1.873 ms
23:04:13.238:    ┗OBSBasic::OBSInit: 469.771 ms
23:04:13.239:      ┣OBSBasic::InitBasicConfig: 0.08 ms
23:04:13.239:      ┣OBSBasic::ResetAudio: 0.157 ms
23:04:13.239:      ┣OBSBasic::ResetVideo: 53.923 ms
23:04:13.239:      ┃ ┗obs_init_graphics: 51.358 ms
23:04:13.239:      ┃   ┗shader compilation: 34.963 ms
23:04:13.239:      ┣OBSBasic::InitOBSCallbacks: 0.003 ms
23:04:13.239:      ┣OBSBasic::InitHotkeys: 0.013 ms
23:04:13.239:      ┣obs_load_all_modules2: 357.69 ms
23:04:13.239:      ┃ ┣obs_init_module(aja-output-ui.so): 0.054 ms
23:04:13.239:      ┃ ┣obs_init_module(aja.so): 0.044 ms
23:04:13.239:      ┃ ┣obs_init_module(decklink-captions.so): 0 ms
23:04:13.239:      ┃ ┣obs_init_module(decklink-output-ui.so): 0 ms
23:04:13.239:      ┃ ┣obs_init_module(decklink.so): 0.07 ms
23:04:13.239:      ┃ ┣obs_init_module(frontend-tools.so): 58.96 ms
23:04:13.239:      ┃ ┣obs_init_module(image-source.so): 0.006 ms
23:04:13.239:      ┃ ┣obs_init_module(linux-capture.so): 0 ms
23:04:13.239:      ┃ ┣obs_init_module(linux-jack.so): 0.004 ms
23:04:13.239:      ┃ ┣obs_init_module(linux-pipewire.so): 8.482 ms
23:04:13.239:      ┃ ┣obs_init_module(linux-pulseaudio.so): 0.001 ms
23:04:13.239:      ┃ ┣obs_init_module(linux-v4l2.so): 8.861 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-browser.so): 0.036 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-ffmpeg.so): 27.795 ms
23:04:13.239:      ┃ ┃ ┗nvenc_check: 1.405 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-filters.so): 0.027 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-libfdk.so): 0.001 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-outputs.so): 0.006 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-qsv11.so): 5.136 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-transitions.so): 0.005 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-vst.so): 0.002 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-webrtc.so): 0.005 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-websocket.so): 2.911 ms
23:04:13.239:      ┃ ┣obs_init_module(obs-x264.so): 0.002 ms
23:04:13.239:      ┃ ┣obs_init_module(rtmp-services.so): 0.799 ms
23:04:13.239:      ┃ ┗obs_init_module(text-freetype2.so): 0.011 ms
23:04:13.239:      ┣OBSBasic::InitService: 1.121 ms
23:04:13.239:      ┣OBSBasic::ResetOutputs: 0.108 ms
23:04:13.239:      ┣OBSBasic::CreateHotkeys: 0.015 ms
23:04:13.239:      ┣OBSBasic::InitPrimitives: 0.073 ms
23:04:13.239:      ┗OBSBasic::Load: 41.036 ms
23:04:13.239: obs_hotkey_thread(25 ms): min=0 ms, median=0 ms, max=0.222 ms, 99th percentile=0.001 ms, 100% below 25 ms
23:04:13.239: audio_thread(Audio): min=0.004 ms, median=0.184 ms, max=1.108 ms, 99th percentile=0.341 ms
23:04:13.239:  ┗receive_audio: min=0.001 ms, median=0.18 ms, max=1.077 ms, 99th percentile=0.319 ms, 0.645742 calls per parent call
23:04:13.239:    ┣buffer_audio: min=0 ms, median=0 ms, max=0.183 ms, 99th percentile=0.001 ms
23:04:13.239:    ┗do_encode: min=0.056 ms, median=0.179 ms, max=1.075 ms, 99th percentile=0.317 ms
23:04:13.239:      ┣encode(simple_aac): min=0.053 ms, median=0.167 ms, max=0.823 ms, 99th percentile=0.275 ms
23:04:13.239:      ┗send_packet: min=0 ms, median=0.005 ms, max=0.86 ms, 99th percentile=0.073 ms
23:04:13.239: obs_graphics_thread(16.6667 ms): min=0.054 ms, median=0.817 ms, max=173.847 ms, 99th percentile=1.376 ms, 99.9983% below 16.667 ms
23:04:13.239:  ┣tick_sources: min=0 ms, median=0.004 ms, max=18.213 ms, 99th percentile=0.011 ms
23:04:13.239:  ┣output_frame: min=0.032 ms, median=0.552 ms, max=10.476 ms, 99th percentile=0.996 ms
23:04:13.239:  ┃ ┣gs_context(video->graphics): min=0.032 ms, median=0.145 ms, max=10.476 ms, 99th percentile=0.372 ms
23:04:13.239:  ┃ ┃ ┣render_video: min=0.005 ms, median=0.075 ms, max=8.626 ms, 99th percentile=0.223 ms
23:04:13.239:  ┃ ┃ ┃ ┣render_main_texture: min=0.003 ms, median=0.056 ms, max=0.835 ms, 99th percentile=0.183 ms
23:04:13.239:  ┃ ┃ ┃ ┣render_convert_texture: min=0.007 ms, median=0.014 ms, max=8.42 ms, 99th percentile=0.025 ms, 0.645756 calls per parent call
23:04:13.239:  ┃ ┃ ┃ ┗stage_output_texture: min=0.004 ms, median=0.008 ms, max=0.362 ms, 99th percentile=0.016 ms, 0.645756 calls per parent call
23:04:13.239:  ┃ ┃ ┣gs_flush: min=0.001 ms, median=0.049 ms, max=1.844 ms, 99th percentile=0.081 ms
23:04:13.239:  ┃ ┃ ┗download_frame: min=0 ms, median=0.027 ms, max=4.103 ms, 99th percentile=0.213 ms, 0.645756 calls per parent call
23:04:13.239:  ┃ ┗output_video_data: min=0.334 ms, median=0.446 ms, max=10.298 ms, 99th percentile=0.747 ms, 0.645755 calls per parent call
23:04:13.239:  ┗render_displays: min=0.001 ms, median=0.223 ms, max=173.672 ms, 99th percentile=0.487 ms
23:04:13.239: video_thread(video): min=1.142 ms, median=1.426 ms, max=16.24 ms, 99th percentile=2.034 ms
23:04:13.239:  ┗receive_video: min=1.142 ms, median=1.425 ms, max=16.239 ms, 99th percentile=2.034 ms
23:04:13.239:    ┗do_encode: min=1.142 ms, median=1.425 ms, max=16.238 ms, 99th percentile=2.033 ms
23:04:13.239:      ┣encode(simple_video_stream): min=1.137 ms, median=1.401 ms, max=16.215 ms, 99th percentile=2 ms
23:04:13.239:      ┗send_packet: min=0.001 ms, median=0.019 ms, max=1.816 ms, 99th percentile=0.096 ms
23:04:13.239: =================================================
23:04:13.239: == Profiler Time Between Calls ==================
23:04:13.239: obs_hotkey_thread(25 ms): min=25.005 ms, median=25.058 ms, max=25.572 ms, 99.9997% within ±2% of 25 ms (0% lower, 0.000264759% higher)
23:04:13.239: obs_graphics_thread(16.6667 ms): min=4.239 ms, median=16.667 ms, max=173.851 ms, 99.9962% within ±2% of 16.667 ms (0.00184908% lower, 0.00193713% higher)
23:04:13.239: =================================================
23:04:13.252: Number of memory leaks: 0
